package me.geoff.android.sadbluetoothcontroller;

import android.view.View;

public class ColorManager {
    int color;
    private Preferences preferences;
    private View view;
    private static final String PREFERENCE = "color";

    public ColorManager(Preferences preferences) {
        this.preferences = preferences;
        this.initialiseColor();
    }

    public void initialiseColorDisplay(View view) {
        this.view = view;
        view.setBackgroundColor(color);
    }

    public void updateColor(int color) {
        this.color = color;
        view.setBackgroundColor(color);
        saveColor();
    }

    public int getColor() { return color; }

    private void initialiseColor() {
        color = preferences.getPreference(PREFERENCE, 0);
    }

    private void saveColor() {
        preferences.savePreference(PREFERENCE, color);
    }
}
