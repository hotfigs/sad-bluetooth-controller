package me.geoff.android.sadbluetoothcontroller;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Set;
import java.util.UUID;

public class BluetoothModule {

    private Context context;
    private BluetoothSocket btSocket = null;
    public static final int REQUEST_ENABLE_BT = 1;
    private static final UUID BT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String BT_DEVICE_NAME = "BTSADLamp";
    private OutputStream outStream;
    private InputStream inStream;

    public static class BluetoothMessage {
        public int color;
        public long secondsToFade;
        public long secondsToStayOn;

        @Override
        public String toString() {
            return "Color: " + color + ", fade: " + secondsToFade + ", stay on: " + secondsToStayOn;
        }
    }

    public BluetoothModule(Context context) {
        Log.d("SAD", "Creating new bluetooth module");
        this.context = context;
    }

    public void close() {
        Log.i("SAD", "Closing bluetooth socket");

        if (inStream != null) {
            try { inStream.close(); } catch (IOException e) { }
        }

        if (outStream != null) {
            try { outStream.close(); } catch (IOException e) { }
        }

        if (btSocket != null) {
            try { btSocket.close(); } catch (IOException e) { }
        }
    }

    public void sendBluetoothMessage(BluetoothMessage message) {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            ToastHelper.show(context, "Bluetooth adapter not available");
            return;
        }
        if (!adapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            // TODO: Remove if this still works, Context used to be an Activity
            //context.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            enableBtIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(enableBtIntent);
        }
        BluetoothDevice device = findBluetoothDevice(BT_DEVICE_NAME, adapter);
        if (device == null) {
            ToastHelper.show(context, "Is not paired with " + BT_DEVICE_NAME);
            return;
        }
        adapter.cancelDiscovery();
        sendBluetoothMessage(device, message);

    }

    private BluetoothDevice findBluetoothDevice(String name, BluetoothAdapter adapter) {
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();
        if (pairedDevices != null) {
            for (BluetoothDevice device : pairedDevices) {
                //ToastHelper.show(context, "Found device: " + device.getName());
                if (device.getName().equals(name)) return device;
            }
            if (pairedDevices.size() == 0) {
                ToastHelper.show(context, "No paired devices");
            }
        }
        return null;
    }

    private void sendBluetoothMessage(BluetoothDevice device, BluetoothMessage message) {
        try {
            BluetoothSocket socket = getBluetoothSocket(device);
            outStream = socket.getOutputStream();
            inStream = socket.getInputStream();
            byte[] bytes = createBytesFromMessage(message);
            ToastHelper.show(context, "Writing to socket");
            outStream.write(bytes);
            outStream.flush();
            ToastHelper.show(context, "Successfully sent message to lamp");
            Thread.sleep(1000);
        } catch (Exception ex) {
            ToastHelper.show(context, "Error sending message: " + ex.getMessage() + " " + ex.getClass().toString());
            Log.e("SAD", "Error sending message", ex);
        }
    }


    private BluetoothSocket getBluetoothSocket(BluetoothDevice device) throws IOException {
        if (btSocket != null) {
            if (btSocket.isConnected()) return btSocket;
        }
        ToastHelper.show(context, "Creating RF Comm socket");
        btSocket = device.createRfcommSocketToServiceRecord(BT_UUID);
        ToastHelper.show(context, "Connecting to bluetooth server");
        btSocket.connect();
        return btSocket;
    }

    private byte[] createBytesFromMessage(BluetoothMessage message) throws IOException {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        // Header
        byteStream.write(0x06);
        byteStream.write(0x85);
        // Size
        byteStream.write(0x0B);
        // Red
        byteStream.write((byte) Color.red(message.color));
        // Green
        byteStream.write((byte) Color.green(message.color));
        // Blue
        byteStream.write((byte) Color.blue(message.color));
        // Seconds to fade
        byteStream.write(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) message.secondsToFade).array());
        // Seconds to stay on
        byteStream.write(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) message.secondsToStayOn).array());

        // This is where the checksum will go
        byteStream.write(0);

        byte[] bytes = byteStream.toByteArray();

        // calculate checksum
        byte cs = (byte) 0x0B;
        for (int i = 3; i < bytes.length - 2; i++) {
            cs ^= bytes[i];
        }
        bytes[bytes.length - 1] = cs;

        return bytes;
    }
}
