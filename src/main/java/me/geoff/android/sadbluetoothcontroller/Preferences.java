package me.geoff.android.sadbluetoothcontroller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    private Context context;
    private static final String PREFS_NAME = "SADPreferences";

    public Preferences(Context context) {
        this.context = context;
    }

    public void savePreference(String id, int value) {
        SharedPreferences settings = getPreferences();
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(id, value);
        editor.commit();
    }

    public int getPreference(String id, int defaultValue) {
        SharedPreferences settings = getPreferences();
        return settings.getInt(id, defaultValue);
    }

    private SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
