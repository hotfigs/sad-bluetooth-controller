package me.geoff.android.sadbluetoothcontroller;

import android.widget.SeekBar;
import android.widget.TextView;

public abstract class TimeSeekbarController {
    private final SeekBar seekbar;
    private final TextView fadeTimeText;

    public TimeSeekbarController(SeekBar seekbar, final TextView fadeTimeText) {
        this.seekbar = seekbar;
        this.fadeTimeText = fadeTimeText;
        this.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                setValue(i);
                setTimeText(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    protected void initialise() {
        int v = getValue();
        setTimeText(v);
        seekbar.setProgress(v);
    }

    private void setTimeText(int i) {
        fadeTimeText.setText(i + " sec");
    }

    protected abstract void setValue(int i);
    protected abstract int getValue();
}

