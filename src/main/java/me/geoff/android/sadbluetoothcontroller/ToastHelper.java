package me.geoff.android.sadbluetoothcontroller;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

class ToastHelper {
    static void show(Context context, String text) {
        if (context != null) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        }
        Log.d("SAD", "Just toasted: " + text);
    }
}
