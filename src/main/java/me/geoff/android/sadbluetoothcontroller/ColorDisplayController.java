package me.geoff.android.sadbluetoothcontroller;

import android.view.View;

public class ColorDisplayController {
    private View view;
    private FaderModel model;

    public ColorDisplayController(FaderModel model, View view) {
        this.model = model;
        this.view = view;
        this.view.setBackgroundColor(model.getColor());
    }

    public void setColor(int color) {
        this.model.setColor(color);
        this.view.setBackgroundColor(color);

    }
}
