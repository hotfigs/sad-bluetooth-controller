package me.geoff.android.sadbluetoothcontroller;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class SadBluetoothService extends IntentService {

    public SadBluetoothService() {
        super("SadBluetoothService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i("SAD", "In bluetooth service");
        Preferences preferences = new Preferences(getApplicationContext());
        FaderModelStorage storage = new FaderModelStorage(preferences);
        FaderModel faderModel = storage.load();

        BluetoothModule.BluetoothMessage message = new BluetoothModule.BluetoothMessage();
        message.color = faderModel.getColor();
        message.secondsToFade = faderModel.getFadeSeconds();
        message.secondsToStayOn = faderModel.getStayOnSeconds();
        Log.d("SAD", "Sending: " + message.toString());
        BluetoothModule module = new BluetoothModule(getApplication());
        module.sendBluetoothMessage(message);
        module.close();
    }
}
