package me.geoff.android.sadbluetoothcontroller;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.commonsware.cwac.colormixer.ColorMixer;
import com.commonsware.cwac.colormixer.ColorMixerDialog;

import java.util.Calendar;

// TODO: Hook up to an alarm clock
// TODO: Log scale on the time slider + nice time formatting
// TODO: Remove ColorManager
// TODO: Trying bluetooth through service breaks it
// TODO: The color picker sets the background color on the wrong control
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DummyFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class DummyFragment extends Fragment implements View.OnClickListener {

        private Preferences preferences;
        private FaderModel faderModel = new FaderModel();
        private TimeSeekbarController timeSeekbarController;
        private FaderModelStorage faderModelStorage;
        private StayOnTimeSeekbarController stayOnSeekbarController;
        private FaderObserver faderObserver;
        private ColorDisplayController colorDisplayController;


        public DummyFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            
            this.preferences = new Preferences(getActivity());
            this.faderModelStorage = new FaderModelStorage(preferences);
            this.faderModel = faderModelStorage.load();
            this.faderObserver = new FaderObserver(faderModelStorage);
            faderModel.addObserver(faderObserver);

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            Button colorPickButton = (Button) rootView.findViewById(R.id.color_pick_button);
            colorPickButton.setOnClickListener(this);

            Button sendButton = (Button) rootView.findViewById(R.id.send_button);
            sendButton.setOnClickListener(this);

            Button setAlarmButton = (Button) rootView.findViewById(R.id.set_alarm_button);
            setAlarmButton.setOnClickListener(this);

            this.colorDisplayController = new ColorDisplayController(faderModel, rootView.findViewById(R.id.color_box));

            SeekBar fadeTimeSeekBar = (SeekBar) rootView.findViewById(R.id.fade_seek_bar);
            TextView fadeTimeText = (TextView) rootView.findViewById(R.id.fade_time_text);
            this.timeSeekbarController = new FadeTimeSeekbarController(faderModel, fadeTimeSeekBar, fadeTimeText);

            SeekBar stayOnTimeSeekbar = (SeekBar) rootView.findViewById(R.id.stay_on_seek_bar);
            TextView stayOnTimeText = (TextView) rootView.findViewById(R.id.stay_on_time_text);
            this.stayOnSeekbarController = new StayOnTimeSeekbarController(faderModel, stayOnTimeSeekbar, stayOnTimeText);

            setNextAlarmText(rootView);

            return rootView;
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.color_pick_button:
                    onColorPickButtonClick();
                    break;
                case R.id.send_button:
                    onSendButtonClick();
                    break;
                case R.id.set_alarm_button:
                    onSetAlarmButton();
                    break;
            }
        }

        private void onSetAlarmButton() {
            TimePickerFragment fragment = new TimePickerFragment() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    faderModel.setAlarmHour(hourOfDay);
                    faderModel.setAlarmMinute(minute);
                    setAlarm();
                }
            };
            fragment.show(getActivity().getSupportFragmentManager(), "timePicker");
        }

        private void onColorPickButtonClick() {
            ColorMixerDialog dialog = new ColorMixerDialog(this.getActivity(), faderModel.getColor(), new ColorMixer.OnColorChangedListener() {
                @Override
                public void onColorChange(int i) {
                    colorDisplayController.setColor(i);
                }
            });
            dialog.show();
        }

        private void onSendButtonClick() {
            Intent intent = new Intent(getActivity(), AlarmReceiver.class);
            getActivity().sendBroadcast(intent);
        }

        private void setNextAlarmText(View view) {
            TextView alarmText = (TextView) view.findViewById(R.id.next_alarm_text);
            String text = faderModel.getAlarmHour() + ":" + faderModel.getAlarmMinute();
            alarmText.setText(text);
        }

        private void setAlarm() {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(System.currentTimeMillis());
            c.set(Calendar.HOUR_OF_DAY, faderModel.getAlarmHour());
            c.set(Calendar.MINUTE, faderModel.getAlarmMinute());
            AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getActivity(), AlarmReceiver.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, 0);
            manager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 1000 * 60 * 60 * 24, alarmIntent);
            Log.i("SAD", "Alarm is set: " + c.toString());
        }
    }
}
