package me.geoff.android.sadbluetoothcontroller;

import java.util.Observable;
import java.util.Observer;

public class FaderObserver implements Observer {

    private FaderModelStorage storage;

    public FaderObserver(FaderModelStorage storage) {
        this.storage = storage;
    }

    @Override
    public void update(Observable observable, Object o) {
        FaderModel model = (FaderModel) observable;
        storage.save(model);
    }
}
