package me.geoff.android.sadbluetoothcontroller;

public class FaderModelStorage {

    private Preferences preferences;
    private static final String FADE_PREFERENCE = "Fade";
    private static final String STAY_ON_PREFERENCE = "StayOn";
    private static final String ALARM_HOUR = "AlarmHour";
    private static final String ALARM_MINUTE = "AlarmMinute";
    private static final String COLOR = "Color";

    public FaderModelStorage(Preferences preferences) {
        this.preferences = preferences;
    }

    public FaderModel load() {
        FaderModel model = new FaderModel();
        model.setFadeSeconds(preferences.getPreference(FADE_PREFERENCE, 0));
        model.setStayOnSeconds(preferences.getPreference(STAY_ON_PREFERENCE, 0));
        model.setAlarmHour(preferences.getPreference(ALARM_HOUR, 0));
        model.setAlarmMinute(preferences.getPreference(ALARM_MINUTE, 0));
        model.setColor(preferences.getPreference(COLOR, 0));
        return model;
    }

    public void save(FaderModel model) {
        preferences.savePreference(FADE_PREFERENCE, model.getFadeSeconds());
        preferences.savePreference(STAY_ON_PREFERENCE, model.getStayOnSeconds());
        preferences.savePreference(ALARM_HOUR, model.getAlarmHour());
        preferences.savePreference(ALARM_MINUTE, model.getAlarmMinute());
        preferences.savePreference(COLOR, model.getColor());
    }
}
