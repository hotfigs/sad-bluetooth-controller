package me.geoff.android.sadbluetoothcontroller;

import android.widget.SeekBar;
import android.widget.TextView;

public class FadeTimeSeekbarController extends TimeSeekbarController {

    private FaderModel model;

    public FadeTimeSeekbarController(FaderModel model, SeekBar seekbar, TextView fadeTimeText) {
        super(seekbar, fadeTimeText);
        this.model = model;
        initialise();
    }

    @Override
    protected void setValue(int i) {
        model.setFadeSeconds(i);
    }

    @Override
    protected int getValue() {
        return model.getFadeSeconds();
    }
}

