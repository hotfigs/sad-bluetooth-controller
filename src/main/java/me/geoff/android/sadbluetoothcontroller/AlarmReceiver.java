package me.geoff.android.sadbluetoothcontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("SAD", "Got the alarm receiver");
        Intent sendBluetoothIntent = new Intent(context, SadBluetoothService.class);
        context.startService(sendBluetoothIntent);
    }
}
