package me.geoff.android.sadbluetoothcontroller;

import java.util.Observable;

public class FaderModel extends Observable {
    public int color;
    public int fadeSeconds;
    public int stayOnSeconds;
    public int alarmHour;
    public int alarmMinute;

    public int getColor() { return color; }
    public int getFadeSeconds() { return fadeSeconds; }
    public int getStayOnSeconds() { return stayOnSeconds; }
    public int getAlarmHour() { return alarmHour; }
    public int getAlarmMinute() { return alarmMinute; }

    public void setColor(int newColor) {
        if (color != newColor) setChanged();
        color = newColor;
        notifyObservers();
    }

    public void setFadeSeconds(int newFadeSeconds) {
        if (fadeSeconds != newFadeSeconds) setChanged();
        fadeSeconds = newFadeSeconds;
        notifyObservers();
    }

    public void setStayOnSeconds(int newStayOnSeconds) {
        if (stayOnSeconds != newStayOnSeconds) setChanged();
        stayOnSeconds = newStayOnSeconds;
        notifyObservers();
    }

    public void setAlarmHour(int newAlarmHour) {
        if (alarmHour != newAlarmHour) setChanged();
        alarmHour = newAlarmHour;
        notifyObservers();
    }

    public void setAlarmMinute(int newAlarmMinute) {
        if (alarmMinute != newAlarmMinute) setChanged();
        alarmMinute = newAlarmMinute;
        notifyObservers();
    }
}
