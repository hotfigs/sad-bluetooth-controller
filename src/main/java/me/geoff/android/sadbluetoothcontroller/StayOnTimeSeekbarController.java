package me.geoff.android.sadbluetoothcontroller;

import android.widget.SeekBar;
import android.widget.TextView;

public class StayOnTimeSeekbarController extends TimeSeekbarController {

    private FaderModel model;

    public StayOnTimeSeekbarController(FaderModel model, SeekBar seekbar, TextView fadeTimeText) {
        super(seekbar, fadeTimeText);
        this.model = model;
        initialise();
    }

    @Override
    protected void setValue(int i) {
        model.setStayOnSeconds(i);
    }

    @Override
    protected int getValue() {
        return model.getStayOnSeconds();
    }
}
