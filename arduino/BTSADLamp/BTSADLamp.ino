#include <EasyTransfer.h>
#include "FastSPI_LED2.h" //used for rgb class :)

#define PIN_RED 6
#define PIN_GREEN 9
#define PIN_BLUE 3

EasyTransfer bluetooth;

struct RECEIVE_DATA_STRUCTURE{
  byte red;
  byte green; 
  byte blue;
  unsigned long secondsToFade;
  unsigned long secondsToStayOn;
};

RECEIVE_DATA_STRUCTURE bluetoothPacket;

const int dimmingSteps = 255; //seems logical since there is a maximum of 255 levels for each colour.
const long switchOffDuration = 300; 

CRGB startColour = CRGB(0,0,0);
CRGB currentColour = CRGB(0,0,0);
CRGB destinationColour = CRGB(0,0,0);
unsigned long dimmingDuration = 0;
unsigned long onDuration = 0;
unsigned int stepNumber = 0;
float stepDelay = 0;
float redIncrement = 0;
float greenIncrement = 0;
float blueIncrement = 0;
unsigned long LEDsLastSet = 0;
unsigned long LEDstartTime = 0; 
boolean needsSwichingOff = false;

void setup() {
  // put your setup code here, to run once:
  //delay(10000);
//  Serial.begin(57600);
//  while (!Serial) {
//  }
  
  Serial1.begin(115200);
  pinMode(PIN_RED,OUTPUT);
  pinMode(PIN_GREEN,OUTPUT);
  pinMode(PIN_BLUE,OUTPUT);
  analogWrite(PIN_RED,255);
  delay(100);
  while (!Serial1) {
     // wait for serial port to connect. Needed for Leonardo only
  }
  bluetooth.begin(details(bluetoothPacket), &Serial1);
  analogWrite(PIN_RED,30);
  analogWrite(PIN_GREEN,30);
  analogWrite(PIN_BLUE,60);
  //Serial1.println("Setup");
  delay(1000);
  initLEDs();
  delay(1000);
  //debugLEDs(3,60,0,255,0);
//  Serial.println("Setup!");
//  Serial.println(sizeof(bluetoothPacket));
}

void loop() {
  doBlueTooth();
  //debugSerial();
  doLEDs();
  delay(1);
}

void debugSerial()
{
  if (Serial1.available())
  {
    byte b = Serial1.read();
//    Serial.println(b);
    //Serial1.read();
    digitalWrite(13,HIGH);
    delay(500);
    digitalWrite(13,LOW);
    delay(500);
  }
}

void doBlueTooth()
{
  if(Serial1.available() > 0)
  {
//    Serial.println("We Have Data");
//    Serial.println(Serial1.available());
    delay(100);
//    Serial.println(Serial1.available());

    if(bluetooth.receiveData())
    {
//      Serial.println("Woohoo!");
//      Serial.println(bluetoothPacket.red);
//      Serial.println(bluetoothPacket.green);
//      Serial.println(bluetoothPacket.blue);
//      Serial.println(bluetoothPacket.secondsToStayOn);
//      Serial.println(bluetoothPacket.secondsToFade);
      onDuration = bluetoothPacket.secondsToStayOn * 1000; //convert to miliseconds
      dimmingDuration = bluetoothPacket.secondsToFade * 1000;  
      destinationColour = CRGB(bluetoothPacket.red,bluetoothPacket.green,bluetoothPacket.blue);
      startColour = CRGB(currentColour.red,currentColour.green,currentColour.blue);
      stepNumber = 0;
      stepDelay = (float) dimmingDuration / (float) dimmingSteps;
      
      redIncrement = (float) (destinationColour.red - currentColour.red) / (float) dimmingSteps;
      greenIncrement = (float) (destinationColour.green - currentColour.green) / (float) dimmingSteps;
      blueIncrement = (float) (destinationColour.blue - currentColour.blue) / (float) dimmingSteps;
      
      needsSwichingOff = true; //this might get added to the packet.
      
      LEDstartTime = millis();
      LEDsLastSet = LEDstartTime;
    }
  }
}

void initLEDs()
{
    analogWrite(PIN_RED,currentColour.red);
    analogWrite(PIN_GREEN,currentColour.green);
    analogWrite(PIN_BLUE,currentColour.blue);  
}

void doLEDs()
{
  //do dimming
  if ( (millis() - LEDsLastSet > (long) stepDelay ) && stepNumber < dimmingSteps ) //delay calculation needs checking, might cause issues when millis() wraps round to zero, somat like every 40 days, Step number also needs checking might be off by 1.
  {
    //LEDs need updating
    stepNumber++;
    LEDsLastSet = millis();
    
    currentColour.red = startColour.red + (int) (redIncrement * stepNumber);
    currentColour.green = startColour.green + (int) (greenIncrement * stepNumber);
    currentColour.blue = startColour.blue + (int) (blueIncrement * stepNumber);
    
    //update the LEDs
    analogWrite(PIN_RED,currentColour.red);
    analogWrite(PIN_GREEN,currentColour.green);
    analogWrite(PIN_BLUE,currentColour.blue);
  }
  
  //check if needs to be switched off
  else if ( ( millis() - LEDstartTime > dimmingDuration + onDuration ) && (stepNumber == dimmingSteps) && (needsSwichingOff == true))
  {
    needsSwichingOff = false;
    onDuration = 0;
    dimmingDuration = switchOffDuration;  
    destinationColour = CRGB(0,0,0);
    startColour = CRGB(currentColour.red,currentColour.green,currentColour.blue);
    stepNumber = 0;
    stepDelay = (float) dimmingDuration / (float) dimmingSteps;
    
    redIncrement = (float) (destinationColour.red - currentColour.red) / (float) dimmingSteps;
    greenIncrement = (float) (destinationColour.green - currentColour.green) / (float) dimmingSteps;
    blueIncrement = (float) (destinationColour.blue - currentColour.blue) / (float) dimmingSteps;
    
    LEDstartTime = millis();
    LEDsLastSet = LEDstartTime;    
  }
}

void debugLEDs(unsigned long secondsToFade, unsigned long secondsToStayOn, byte red, byte green, byte blue)
{
    onDuration = secondsToStayOn * 1000; //convert to miliseconds
    dimmingDuration = secondsToFade * 1000;  
    destinationColour = CRGB(red,green,blue);
    startColour = CRGB(currentColour.red,currentColour.green,currentColour.blue);
    stepNumber = 0;
    stepDelay = (float) dimmingDuration / (float) dimmingSteps;
    
    redIncrement = (float) (destinationColour.red - currentColour.red) / (float) dimmingSteps;
    greenIncrement = (float) (destinationColour.green - currentColour.green) / (float) dimmingSteps;
    blueIncrement = (float) (destinationColour.blue - currentColour.blue) / (float) dimmingSteps;
    
    needsSwichingOff = true; //this might get added to the packet.
    
    LEDstartTime = millis();
    LEDsLastSet = LEDstartTime;
}
